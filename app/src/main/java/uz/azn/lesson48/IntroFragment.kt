package uz.azn.lesson48

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.work.*
import uz.azn.lesson48.databinding.FragmentIntroBinding
import java.util.concurrent.TimeUnit


class IntroFragment : Fragment() {
    private lateinit var binding: FragmentIntroBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED) //  internetni tekshiradi
//            .setRequiresCharging(true)  //  zaydaykani tekshiradi
            .setRequiresBatteryNotLow(true) // batarayka kam bolsa  ishlashi
            .setRequiresStorageNotLow(true) // bu hotirada joy bor yoqligini tekshiradi
            .setRequiresDeviceIdle(true) // yani bu telefoni qotib yoki qotmasadan ishlashi
            .build()
        // data malumotni  oladi
        val data = Data.Builder().apply {
            putString("hi", "Salom")
            putString("description", "Tasvirlash")
            putString("title", "Sarlavha")
        }
// bir marta jonatadi
        val workerRequest = OneTimeWorkRequestBuilder<MyTask>()
            .setConstraints(constraints)
            .setInputData(data.build())
            .setInitialDelay(2,TimeUnit.HOURS) //  qaysi vaqtda jonatsin deganini  bildiradi 
            .addTag("My_task")
            .setInputData(workDataOf("name" to "zokirjon", "age" to 21))
            .build()

        WorkManager.getInstance(requireContext())
            .cancelAllWork() // bu  hamma workManagerlarni ochiradi
        WorkManager.getInstance(requireContext())
            .cancelAllWorkByTag("My_Task ") //  bu shu tag bolsa toxtadi  yani biz notification ochaydigon knopkaga yozsek ochadi
        WorkManager.getInstance(requireContext())
            .enqueue(workerRequest) // yani keyingisin bajarihs uchun ochiritda turibdi deganini bildiradi
        WorkManager.getInstance(requireContext()).getWorkInfoByIdLiveData(workerRequest.id)
            .observe(requireActivity(),
                Observer {
                    Log.d("My Worker", it.state.toString())  // yani bu natija qaytishini korish uchun ishlatiladi hohlatlari bor
                })
        /**
         * u bir kunda bitta vaqtda jonatish ishlatiladi
        val workerRequest2 = PeriodicWorkRequestBuilder<MyTask>(1,TimeUnit.DAYS)
        WorkManager.getInstance(requireContext()).enqueue(workerRequest2.build())
         */

//         bu doWorkni ishga tushurib beradi
        WorkManager.getInstance(requireContext()).enqueue(workerRequest)

    }
}