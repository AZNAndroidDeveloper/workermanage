package uz.azn.lesson48

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters

class MyTask(context: Context, parameters: WorkerParameters):Worker(context,parameters){
    override fun doWork(): Result {
        val name = inputData.getString("name")?:return  Result.failure()
        Log.d("My task ", " My work has started")
        Log.d("name", name)


        return Result.success()
    }

}